@extends('admin.layouts.layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid mb-2">
            <h1>New category</h1>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Edit category</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>

            <form role="form" method="post" action="{{route('categories.store')}}">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Name</label>
                        <input type="text" class="form-control" name="title" id="title" autofocus
                               @error('title') is-invalid @enderror>
                    </div>

                    <div class="form-group">
                        <div class="icheck-secondary">
                            <input type="checkbox" name="updateSlug" id="updateSlug">
                            <label for="updateSlug">Update slug</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input type="text" class="form-control" name="slug" id="slug"
                               @error('slug') is-invalid @enderror>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

            <!--<div class="card-footer clearfix"></div>-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection
